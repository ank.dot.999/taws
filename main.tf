provider "aws" {

	region="us-east-1"

}

resource "aws_vpc" "development-vpc"{
	cidr_block="10.0.0.0/16"
	tags={
		Name = "development"
		vpc_env="dev"
	}
}

variable "subnet_cidr_block"{
	description ="subnet cidr block "
}

resource "aws_subnet" "dev-subnet-1"{
	vpc_id=aws_vpc.development-vpc.id
	cidr_block=var.subnet_cidr_block
	availability_zone="us-east-1a"
	tags={
		Name="dev-subnet-1-dev"
		subnet_env="dev"
	}
}


data "aws_vpc" "existing_vpc"{
	default=true 

}

resource "aws_subnet" "dev-subnet-2"{
	vpc_id=data.aws_vpc.existing_vpc.id
	cidr_block="172.31.128.0/20"
	availability_zone="us-east-1a"
	tags={	
	Name ="default-subnet"
	}
}

output "dev-vpc-id"{
	value=aws_vpc.development-vpc.id

}

output "dev-subnet-id"{
	value =aws_subnet.dev-subnet-1.id
}

